# UserService - st-2020 BB

## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Setup](#setup)
* [Structure](#user-structure)
* [Methodes](#methods)

## General info
This service provides a restful API to create, retreive, update and delete Users as well as JWT authentication.
By Marlene Frauscher, Ruppert Kozak
	
## Technologies
Project is created with:
* Java OpenJDK
* Spring Boot
* H2 DB
* encryption alogorythm: RSA

Containerization
* Docker Container
* openjdk:8-jre-alpine
	
## Setup

To build the container:
```
$ docker build -t sst-ws2020/user-service:0.0.1-SNAPSHOT -t sst-ws2020/user-service:latest .
```

To run the service:

```

$ docker run -p 8080:8080  sst-ws2020/user-service:latest

```

## User Structure

| Attribute  | Type | Info |
| -----------| -----| ---- |
| id  | Long  | Auto-ID |
| email  | String  | nullable = false, unique = true |
| firstname  | String  | |
| lastname  | String  | |
| password  | String  | nullable = false |

## REST API

The REST API to the user service is described below and documented via Spring Doc Open Api.

Download the attached UserServiceApi-docs.json file, got to https://editor.swagger.io/.
Click then on "File" - "Import file" and insert the UserServiceApi-docs.json. 

Then you can see the detailed API Documentation. 


## Public Key

MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAJIsF5M9qLMPKo2Ls+rilVLYREr6uYr4
Xw6A++oy1dSr+PPTTIuX6sbePjbM6a6Y5G12ssXcDvbesVgzn4PKANsCAwEAAQ==

### Create new User

`POST /api/v1/user/`

    curl -i -H 'Accept: application/json' http://localhost:8080/user/


### Get details of an user

`GET /api/v1/user/:Id`

    curl -i -H 'Accept: application/json' http://localhost:8080/user/
