package com.sst.bb.userservice.userservice.service;

import com.sst.bb.userservice.userservice.controller.LoginDTO;
import com.sst.bb.userservice.userservice.domain.User;
import com.sst.bb.userservice.userservice.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class AuthenticationService {

    private UserRepository userRepository;
    private AuthenticationManager authenticationManager;
    private JwtService jwtService;

    @Autowired
    public AuthenticationService(UserRepository userRepository, AuthenticationManager authenticationManager, JwtService jwtService) {
        this.userRepository = userRepository;
        this.authenticationManager = authenticationManager;
        this.jwtService = jwtService;
    }

    public String login(LoginDTO loginDTO) {

        User user = userRepository.findByEmail(loginDTO.getEmail())
                .orElseThrow(() -> new UsernameNotFoundException(loginDTO.getEmail()));

        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(user.getEmail(), loginDTO.getPassword()));
        Map<String, Object> claims = getClaims(user.getRoles());
        return jwtService.generateToken(user.getEmail(), claims);
    }

    private Map<String, Object> getClaims(List<String> roles) {
        Map<String, Object> claims = new HashMap<>();
        claims.put("roles", String.join(",", roles));
        return claims;
    }

}
