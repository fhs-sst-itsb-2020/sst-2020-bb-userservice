package com.sst.bb.userservice.userservice.controller;

import com.sst.bb.userservice.userservice.domain.User;
import com.sst.bb.userservice.userservice.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/user")
public class UserController {

    private UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }


    @GetMapping("/{userId}")
    public User getById(@PathVariable long userId) {
        return userService.findById(userId);
    }

    @PostMapping("/signup")
    @ResponseStatus(HttpStatus.CREATED)
    public User addNew(@RequestBody SignupDTO newUser) {
        return userService.addNew(newUser);
    }

    @PutMapping("/{userId}")
    public User updateForId(@PathVariable long userId, @RequestBody User newValues) {
        return userService.updateExisting(userId, newValues);
    }

    @DeleteMapping("/{userId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteForId(@PathVariable long userId) {
        userService.deleteById(userId);
    }

}


