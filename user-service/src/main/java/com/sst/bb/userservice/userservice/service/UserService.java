package com.sst.bb.userservice.userservice.service;

import com.sst.bb.userservice.userservice.controller.SignupDTO;
import com.sst.bb.userservice.userservice.domain.User;
import com.sst.bb.userservice.userservice.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

@Service
public class UserService {

    private UserRepository userRepository;
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    public UserService(UserRepository userRepository, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userRepository = userRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    public User findById(long userId) {
        return userRepository.findById(userId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Couldn't find user for ID " + userId));
    }

    public User addNew(SignupDTO signupDTO) {
        User user = User.builder()
                .email(signupDTO.getEmail())
                .firstname(signupDTO.getFirstname())
                .lastname(signupDTO.getLastname())
                .password(bCryptPasswordEncoder.encode(signupDTO.getPassword()))
                .roles(signupDTO.getRoles())
                .build();
        return userRepository.save(user);
    }

    public User updateExisting(long userId, User newValues) {
        User user = findById(userId);
        user.setFirstname(newValues.getFirstname());
        user.setLastname(newValues.getLastname());
        user.setEmail(newValues.getEmail());
        return userRepository.save(user);
    }

    public void deleteById(long userId) {
        User user = findById(userId);
        userRepository.delete(user);
    }
}