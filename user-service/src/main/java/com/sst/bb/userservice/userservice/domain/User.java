package com.sst.bb.userservice.userservice.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, unique = true)
    private String email;

    private String firstname;

    private String lastname;

    @ManyToMany
    @JoinTable(
            name = "user_role", uniqueConstraints = @UniqueConstraint(columnNames = {"user_id", "role_id"}),
            joinColumns = {@JoinColumn(name = "user_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "role_id", referencedColumnName = "id")}
    )
    private List<Role> roles = new ArrayList<>();

    @Column(nullable = false)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;

    public List<String> getRoles() {
        return this.roles.stream().map(Role::getId).collect(Collectors.toList());
    }

    public void setRoles(List<String> roles) {
        this.roles = roles.stream().map(Role::new).collect(Collectors.toList());
    }


    public static class UserBuilder {
        private List<Role> roles;

        public UserBuilder roles(List<String> roles) {
            this.roles = roles.stream().map(Role::new).collect(Collectors.toList());
            return this;
        }
    }
}
