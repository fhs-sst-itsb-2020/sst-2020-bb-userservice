package com.sst.bb.userservice.userservice.controller;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class SignupDTO {
    private String email;
    private String password;
    private String firstname;
    private String lastname;
    private List<String> roles;
}
